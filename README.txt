How to configure:

1. Navigate to admin/store/settings/payment/edit/gateways and open the Authorize.net Multi settings fieldset
2. Enter the number of profiles (ie. Authorize.net accounts) you would like to use and click Save configuration
3. Click the unnamed profile; configure API Login ID, Transaction Key, Transaction Mode, and other settings. Click Submit.
4. Do this for all added profiles
5. Create a product node. Under Product information, there is a Payment Gateway Profile dropdown; select the appropriate gateway.
6. ???
7. Profit
